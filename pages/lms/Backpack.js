import React, {Component, Fragment} from 'react';
import {FlatList, Text, View} from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import BaseStyles from "../../styles/base";
import Header from "../../components/HeaderLMS";
import ListItem from "../../components/backpack/ListItem";
import {dataFile} from "./dummy/dataku";

class Backpack extends Component{

    constructor(props){
        super(props);
    }

    state = {
        exit: false,
        fetching: false,
        error: false,
        loaded: false,
        topic: {},
        content: [],
    }

    componentDidMount(){

    }

    render(){
        return (
            <Fragment>
                <AnimatableView style={ BaseStyles.container }>
                    <Header title="Backpack" logo={true}/>
                    <FlatList data={dataFile.data}
                              renderItem={ ({item}) => <ListItem item={item} withFeatured={true}/>}
                              keyExtractor={item => item.key}
                    />
                </AnimatableView>
            </Fragment>
        );
    }
}

export default Backpack;