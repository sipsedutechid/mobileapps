import React, { Component } from 'react';
import { FlatList, Text, TextInput, View } from 'react-native';
import { BackButton } from 'react-router-native';
import { View as AnimatableView } from 'react-native-animatable';

import Articles from '../../../api/articles';
import * as StyleConstants from '../../../styles/constants';
import BaseStyles from '../../../styles/base';
import Header from '../../../components/Header';
import ListItem from '../../../components/articles/ListItem';
import Error from '../../../components/Error';
import Spinner from '../../../components/Spinner';

class Search extends Component {
  state = {
    fetching: false,
    error: false,
    term: '',
  }
  charThreshold = 4;

  componentDidMount() {
    this.refs.termInput.focus();
  }

  search({ term }) {
    this.setState({ term });
    if (term.length < this.charThreshold)
      return false;

    this.setState({ fetching: true, error: false });
    Articles.index({
      params: { term },
      cb: ({ articles }) => {
        this.props.populate({ articles });
        this.setState({ fetching: false });
      },
      err: () => {
        this.setState({ fetching: false, error: true });
      }
    });
  }

  render() {
    return (
      <AnimatableView animation="fadeIn" duration={ 300 } easing="ease-in-out" style={ BaseStyles.overlayContainer }>
        <Header
          back="/articles"
          titleComponent={
            (
              <TextInput
                ref="termInput"
                placeholder="Cari artikel.."
                underlineColorAndroid={ StyleConstants.COLOR_GRAY.LIGHTEST }
                onSubmitEditing={ e => this.search({ term: e.nativeEvent.text }) }
                style={{
                  fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF,
                  fontSize: StyleConstants.FONT_SIZE.H3,
                  marginTop: StyleConstants.SPACING_BASE
                }} />
            )
          }
          bgStyle="light" />

        { this.state.fetching && <Spinner /> }
        {
          this.state.error ?
            <Error onRetry={ () => this.search({ term: this.state.term }) } /> :
            (
              <FlatList
                data={ this.props.articles }
                keyExtractor={ item => item.slug }
                renderItem={ ({ item, index }) => <ListItem item={ item } index={ index } linkQuery="ref=/articles/search" /> } />
            )
        }

        {
          this.state.term.length && !this.props.articles.length && !this.state.fetching ?
          (
            <View style={{ flex: 1 }}>
              <Text style={{ textAlign: 'center', fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, color: StyleConstants.COLOR_GRAY.NORMAL }}>
                Ups, kami tidak menemukan artikel dengan pencarian tersebut. Silakan coba lagi.
              </Text>
            </View>
          ) : null
        }
        <BackButton />
      </AnimatableView>
    );
  }
}

export default Search;