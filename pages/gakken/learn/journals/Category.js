import React, { Component, Fragment } from 'react';
import { BackHandler, FlatList, Text, View } from 'react-native';
import { Redirect } from 'react-router-native';
import { View as AnimatableView } from 'react-native-animatable';
import Query from 'query-string';
import { CancelToken } from 'axios';

import JournalsApi from '../../../../api/journals';
import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';
import Header from '../../../../components/Header';
import Spinner from '../../../../components/Spinner';
import ListItem from '../../../../components/journals/ListItem';
import Error from '../../../../components/Error';

class Category extends Component {
  state = {
    journals: [],
    fetching: false,
    error: false,
    exit: false,
    loaded: false,
  };
  cancelToken = false;
  exit = this._exit.bind(this);

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.exit);
    this.load();
  }

  _exit() {
    this.refs.baseView.fadeOutRight(300);
    this.cancelToken && this.cancelToken();
    BackHandler.removeEventListener('hardwareBackPress', this.exit);
    setTimeout(() => {
      this.setState({ exit: true });
    }, 300);
    return true;
  }

  load() {
    if (this.state.fetching || this.state.loaded)
      return;

    this.setState({ fetching: true, error: false });
    JournalsApi.categoriesShow({
      slug: this.props.match.params.slug,
      cb: journals => {
        this.cancelToken = false;
        this.setState({ journals, fetching: false, loaded: true });
      },
      err: () => {
        this.cancelToken = false;
        this.setState({ error: true, fetching: false });
      },
      cancelToken: new CancelToken(c => { this.cancelToken = c; })
    });
  }

  render() {
    if (this.state.exit)
      return <Redirect to="/learn/journals" />;

    return (
      <AnimatableView ref="baseView" style={ BaseStyles.overlayContainer } animation="fadeInRight" duration={ 300 }>
        <Header back={ () => this._exit() } titleComponent={
          (
            <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
              <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_HEADINGS, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>Cari Jurnal</Text>
              <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>
                {
                  this.state.fetching ?
                    'Memuat..' :
                    Query.parse(this.props.location.search).categoryName
                }
              </Text>
            </View>
          )
        } />

        { this.state.fetching && <Spinner /> }
        { this.state.error && <Error onRetry={ () => this.load() } /> }
        {
          this.state.loaded &&
          <FlatList
            data={ this.state.journals }
            keyExtractor={ item => item.slug }
            renderItem={ ({item, index}) =>
              <ListItem
                item={ item }
                index={ index }
                linkQuery={"ref=/learn/journals/categories/" + this.props.match.params.slug } />
            } />
        }
      </AnimatableView>
    );
  }
}

export default Category;