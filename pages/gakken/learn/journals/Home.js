import React from 'react';
import { Image, ScrollView, Text, View } from 'react-native';
import { Link, Route } from 'react-router-native';
import { View as AnimatableView } from 'react-native-animatable';
import Touchable from 'react-native-platform-touchable';

import Categories from './HomeCategories';
import * as StyleConstants from '../../../../styles/constants';
import BaseStyles from '../../../../styles/base';
import List from './List';

const Home = props => {
  let animation = 'fadeIn';
  if (props.origin)
    animation = props.origin === 'p2kb' ? 'fadeInRight' : 'fadeInLeft';

  return (
    <AnimatableView animation={ animation } duration={ 300 } easing="ease-in-out-sine" style={{ flex: 1, width: '100%' }}>
      <ScrollView style={{ flex: 1, width: '100%' }}>

        <View style={{ height: StyleConstants.SPACING.X5 }} />

        <View style={ BaseStyles.content }>
          <Link
            to="/learn/journals/all"
            component={ Touchable }
            style={{
              backgroundColor: StyleConstants.COLOR.WHITE,
              paddingHorizontal: StyleConstants.SPACING.X3,
              paddingVertical: StyleConstants.SPACING.X3,
              borderRadius: StyleConstants.SPACING.X2,
              elevation: 2,
              shadowColor: StyleConstants.COLOR.BLACK,
              shadowOpacity: 0.2,
              shadowOffset: { height: 1, width: 1 }
            }}>
            <View style={{ justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row' }}>
              <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, color: StyleConstants.COLOR_GRAY.LIGHT }}>Cari dari semua jurnal..</Text>
              <Image source={ require('../../../../resources/images/Search-Muted.png') } style={{ height: 20, width: 20 }} />
            </View>
          </Link>
        </View>

        <View style={{ height: StyleConstants.SPACING.X3 }} />

        <Categories { ...props.journalCategories } getCategories={ () => props.getJournalCategories() } />
        <View style={{ height: StyleConstants.SPACING.X5 }} />
      </ScrollView>

    </AnimatableView>
  );
}

export default Home;