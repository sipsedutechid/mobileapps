import React, { Component } from 'react';
import { View as AnimatableView } from 'react-native-animatable';
import { FlatList, TextInput, View } from 'react-native';
import { CancelToken } from 'axios';

import JournalsApi from '../../../../api/journals';
import Header from '../../../../components/Header';
import Spinner from '../../../../components/Spinner';
import Error from '../../../../components/Error';
import ListItem from '../../../../components/journals/ListItem';
import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fetching: false,
      error: false,
      term: '',
      journals: [],
    };
  }
  charThreshold = 3;
  cancelToken = false;

  componentDidMount() {
    this.refs.termInput.focus();
  }

  componentWillUnmount() {
    this.cancelToken && this.cancelToken();
  }

  search({ term }) {
    this.setState({ term });
    if (term.length < this.charThreshold)
      return false;

    this.setState({ fetching: true, error: false });
    JournalsApi.index({
      cb: journals => {
        this.setState({ journals, fetching: false });
        this.cancelToken = false;
      },
      err: () => {
        this.setState({ error: true, fetching: false });
        this.cancelToken = false;
      },
      params: {
        term
      },
      cancelToken: new CancelToken(c => { this.cancelToken = c; })
    });
  }

  render() {
    return (
      <AnimatableView style={ BaseStyles.overlayContainer } animation="fadeInRight" duration={ 300 }>
        <Header
          back="/learn"
          titleComponent={
            (
              <TextInput
                ref="termInput"
                placeholder="Cari jurnal.."
                underlineColorAndroid={ StyleConstants.COLOR_GRAY.LIGHTEST }
                onSubmitEditing={ e => this.search({ term: e.nativeEvent.text }) }
                style={{
                  fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF,
                  fontSize: StyleConstants.FONT_SIZE.H3,
                  marginTop: StyleConstants.SPACING_BASE
                }} />
            )
          }
          bgStyle="light"
          />

          { this.state.fetching && <Spinner /> }
          {
            this.state.error ?
              <Error onRetry={ () => this.search({ term: this.state.term }) } /> :
              (
                <FlatList
                  data={ this.state.journals }
                  keyExtractor={ item => item.slug }
                  renderItem={ ({ item, index }) => <ListItem item={ item } index={ index } linkQuery="ref=/learn/journals/all" /> } />
              )
          }
      </AnimatableView>
    );
  }
}

export default List;