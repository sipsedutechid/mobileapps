import React, { Component } from 'react';
import { View } from 'react-native';
import { WebView } from 'react-native-webview';
import Spinner from '../../../../../components/Spinner';

import { MOODLE_API_APP, MOODLE_API_APP_KEY } from '../../../../../env';

class QuizContentView extends Component {
  state = {
    loading: false
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        {
          this.state.loading &&
          (
            <View style={{ width: '100%', alignItems: 'center' }}>
              <Spinner />
            </View>
          )
        }
        <WebView
          source={{
            uri: this.props.data,
            headers: {
              'API-APP': MOODLE_API_APP,
              'API-APP-KEY': MOODLE_API_APP_KEY
            },
          }}
          onLoadStart={ () => this.setState({ loading: true }) }
          onLoadEnd={ () => this.setState({ loading: false }) } />
      </View>
    );
  }
}

export default QuizContentView;