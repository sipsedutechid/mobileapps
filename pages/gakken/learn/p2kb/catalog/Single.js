import React, { Component } from 'react';
import { BackHandler, Image, ScrollView, Text, View } from 'react-native';
import { Link, Redirect } from 'react-router-native';
import { View as AnimatableView } from 'react-native-animatable';
import Query from 'query-string';
import HTMLView from 'react-native-htmlview';
import { CancelToken } from 'axios';

import TopicsApi from '../../../../../api/topics';
import Header from '../../../../../components/Header';
import BaseStyles from '../../../../../styles/base';
import * as StyleConstants from '../../../../../styles/constants';
import Spinner from '../../../../../components/Spinner';
import AuthPrompt from '../../../../../components/AuthPrompt';

class Single extends Component {
  state = {
    topic: {
      title: 'Memuat..'
    },
    fetching: false,
    error: false,
    loaded: false,
    exit: false,
  };
  exit = this._exit.bind(this);
  cancelToken = false;

  componentDidMount() {
    this.load();
    BackHandler.addEventListener('hardwareBackPress', this.exit);
  }

  load() {
    if (this.state.fetching || this.state.loaded)
      return;

    this.setState({ fetching: true, error: false });
    TopicsApi.catalogShow({
      slug: this.props.route.match.params.slug,
      cb: topic => {
        this.setState({ topic, fetching: false, loaded: true });
        this.cancelToken = false;
      },
      err: () => {
        this.setState({ fetching: false, error: true });
        this.cancelToken = false;
      },
      cancelToken: new CancelToken(c => { this.cancelToken = c; })
    });
  }

  _exit() {
    BackHandler.removeEventListener('hardwareBackPress', this.exit);
    this.cancelToken && this.cancelToken();
    this.refs.baseView.fadeOutRight(200);
    setTimeout(() => this.setState({ exit: true }), 200);
    return true;
  }

  render() {
    if (this.state.exit)
      return <Redirect to={ Query.parse(this.props.route.location.search).ref ? Query.parse(this.props.route.location.search).ref : '/learn' } />;

    return (
      <AnimatableView
        ref="baseView"
        style={ BaseStyles.overlayContainer }
        animation="fadeInRight"
        easing="ease-in-out-sine"
        duration={ 300 }>
        <Header
          titleComponent={
            (
              <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
                <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_HEADINGS, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>Katalog GakkenP2KB</Text>
                <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>
                  {
                    this.state.fetching ?
                      'Memuat..' :
                      'Topik P2KB ' + this.state.topic.release
                  }
                </Text>
              </View>
            )
          }
          back={ () => this._exit() } />
          { this.state.fetching && <View style={{ flex: 1 }}><Spinner /></View> }
          {
            this.state.loaded && (
              <ScrollView style={{ flex: 1 }}>
                <Image style={{ width: '100%', height: 235, marginBottom: StyleConstants.SPACING.X4 }} source={{ uri: this.state.topic.featuredImage }} />
                <View style={ BaseStyles.content }>
                  <Text style={ BaseStyles.muted }>{ this.state.topic.release }, oleh { this.state.topic.lecturer }</Text>
                  <Text style={ BaseStyles.h2 }>{ this.state.topic.title }</Text>

                  <HTMLView value={ this.state.topic.summary } stylesheet={ summaryStyle } />
                  <View style={{ height: StyleConstants.SPACING.X4 }} />

                  <Text style={ BaseStyles.h4 }>Anda akan mempelajari</Text>
                  {
                    this.state.topic.content.data.map((content, index) => (
                      <View key={ content.slug } style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                        { index === 0 && <Image source={ require('../../../../../resources/images/TrackerStart-Dark.png') } style={{ height: 40, width: 20 }} /> }
                        { index > 0 && index < (this.state.topic.content.data.length - 1) && <Image source={ require('../../../../../resources/images/TrackerMiddle-Dark.png') } style={{ height: 40, width: 20 }} /> }
                        { index === (this.state.topic.content.data.length - 1) && <Image source={ require('../../../../../resources/images/TrackerEnd-Dark.png') } style={{ height: 40, width: 20 }} /> }
                        <View style={{ width: StyleConstants.SPACING.X2 }} />
                        <Text style={ BaseStyles.textInline } numberOfLines={ 1 }>{ content.title }</Text>
                      </View>
                    ))
                  }

                  <View style={{ height: 50 }} />
                </View>

              </ScrollView>
            )
          }
        <AuthPrompt
          authenticated={ this.props.auth.access.length }
          content="Konten GakkenP2KB"
          action={ "/learn/p2kb/" + this.state.topic.slug + '/engage' }
          actionText="Mulai Belajar"
          disabled={ this.state.fetching || this.state.error }
          disabledText="Memuat.." />
      </AnimatableView>
    );
  }
}

export default Single;

const summaryStyle = {
  p: {
    fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF,
    fontSize: StyleConstants.FONT_SIZE_BASE,
    lineHeight: StyleConstants.FONT_SIZE_BASE * StyleConstants.LINE_HEIGHT_BASE,
    color: StyleConstants.COLOR_GRAY.DARK,
  }
}