import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import Touchable from 'react-native-platform-touchable';

import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';

const tabs = [
  { page: 'overview', label: 'Ringkasan' },
  { page: 'composition', label: 'Komposisi' },
  { page: 'dosages', label: 'Dosis' },
  { page: 'packaging', label: 'Kemasan/Harga' },
  { page: 'related', label: 'Obat Terkait' },
];

const SingleTabs = props => (
  <ScrollView style={ BaseStyles.topNavbar } horizontal={ true } showsHorizontalScrollIndicator={ false }>
    <View style={{ flexDirection: 'row', marginHorizontal: StyleConstants.SPACING.X3 }}>
      {
        tabs.map((tab, index) => (
          <Touchable
            key={ index }
            onPress={ () => props.onChange({ activePage: tab.page }) }
            style={ props.active === tab.page ? BaseStyles.topNavbarItemActive : BaseStyles.topNavbarItem }>
            <View style={{ paddingHorizontal: StyleConstants.SPACING.X2 }}><Text style={ BaseStyles.topNavbarItemText }>{ tab.label }</Text></View>
          </Touchable>
        ))
      }
    </View>
  </ScrollView>
);

export default SingleTabs;