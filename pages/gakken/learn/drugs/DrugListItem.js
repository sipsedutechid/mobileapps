import React from 'react';
import { Text, View } from 'react-native';
import { Link } from 'react-router-native';
import Touchable from 'react-native-platform-touchable';

import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';

const DrugListItem = props => (
  <Link
    to={ "/learn/drugs/" + props.slug + '?ref=' + props.referrer }
    component={ Touchable }
    style={{
      borderTopColor:   StyleConstants.COLOR_GRAY.LIGHTEST,
      borderTopWidth:   1,
      paddingVertical:  StyleConstants.SPACING.X3,
    }}>
    <View style={ BaseStyles.content}>
      <Text style={{
        color:      StyleConstants.COLOR_GRAY.DARK,
        fontSize:   StyleConstants.FONT_SIZE.H5,
        fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF,
      }}>{ props.name }</Text>
      <Text style={ BaseStyles.mutedSmall }>{ props.manufacturer }</Text>
    </View>
  </Link>
);

export default DrugListItem;