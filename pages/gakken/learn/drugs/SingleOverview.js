import React, { Fragment } from 'react';
import { Text, View } from 'react-native';

import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';

const SingleOverview = props => (
  <View style={ BaseStyles.content }>
    <View style={{ marginBottom: StyleConstants.SPACING.X3 }}>
      <Text style={ BaseStyles.mutedSmall }>Indikasi</Text>
      <Text style={ BaseStyles.text }>{ props.indication.length ? props.indication : 'Tidak ada' }</Text>
    </View>
    <View style={{ marginBottom: StyleConstants.SPACING.X3 }}>
      <Text style={ BaseStyles.mutedSmall }>Kontra-Indikasi</Text>
      <Text style={ BaseStyles.text }>{ props.contraindication.length ? props.contraindication : 'Tidak ada' }</Text>
    </View>
    <View style={{ marginBottom: StyleConstants.SPACING.X3 }}>
      <Text style={ BaseStyles.mutedSmall }>Perhatian Khusus</Text>
      <Text style={ BaseStyles.text }>{ props.precautions.length ? props.precautions : 'Tidak ada' }</Text>
    </View>
    <View style={{ marginBottom: StyleConstants.SPACING.X3 }}>
      <Text style={ BaseStyles.mutedSmall }>Efek Samping</Text>
      <Text style={ BaseStyles.text }>{ props.adverseReactions.length ? props.adverseReactions : 'Tidak ada' }</Text>
    </View>
    <View style={{ marginBottom: StyleConstants.SPACING.X3 }}>
      <Text style={ BaseStyles.mutedSmall }>Interaksi Obat</Text>
      <Text style={ BaseStyles.text }>{ props.interactions.length ? props.interactions : 'Tidak ada' }</Text>
    </View>
    <View style={{ marginBottom: StyleConstants.SPACING.X3 }}>
      <Text style={ BaseStyles.mutedSmall }>Sediaan</Text>
      <Text style={ BaseStyles.text }>{ props.forms.length ? props.forms.map(form => form.name).join(', ') : 'Tidak ada' }</Text>
    </View>
  </View>
);

export default SingleOverview;