import React from 'react';
import { Text, View } from 'react-native';

import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';

const SingleComposition = props => {

  return (
    <View style={ BaseStyles.content }>
      {
        props.generics.map((drugGeneric, index) => {
          let mutedText = [];
          if (props.forms.length > 1)
            mutedText.push(drugGeneric.form ? 'Pada sediaan ' + drugGeneric.form.name : 'Pada semua jenis sediaan');
          if (drugGeneric.quantity && drugGeneric.unit)
            mutedText.push((props.forms.length > 1 ? 'sebanyak ' : '') + drugGeneric.quantity + drugGeneric.unit);
          return (
            <View key={ index } style={{ marginBottom: StyleConstants.SPACING.X3 }}>
              <Text style={ BaseStyles.text }>{ drugGeneric.generic.name }</Text>
              <Text style={ BaseStyles.mutedSmall }>
                { mutedText.join(', ') }
              </Text>
            </View>
          );
        })
      }
    </View>
  );
}

export default SingleComposition;