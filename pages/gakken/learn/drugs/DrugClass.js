import React, { Component, Fragment } from 'react';
import { BackHandler, ScrollView, Text, View } from 'react-native';
import { Redirect } from 'react-router-native'
import { View as AnimatableView } from 'react-native-animatable';

import Header from '../../../../components/Header';
import SubclassListItem from './SubclassListItem';
import BaseStyles from '../../../../styles/base';
import * as StyleConstants from '../../../../styles/constants';
import DrugListItem from './DrugListItem';

class DrugClass extends Component {
  state = {
    exit: false,
    loaded: false,
    drugClass: {},
  }
  exit = this._exit.bind(this);

  componentDidMount() {
    let drugClass = this.props.data.filter(drugClass => drugClass.code === this.props.match.params.slug)[0];
    this.setState({
      drugClass,
      loaded: true
    });
    BackHandler.addEventListener('hardwareBackPress', this.exit);
  }

  _exit() {
    BackHandler.removeEventListener('hardwareBackPress', this.exit);
    this.refs.baseView.fadeOutRight(200);
    setTimeout(() => {
      this.setState({ exit: true });
    }, 200);
    return true;
  }

  render() {
    if (this.state.exit)
      return <Redirect to="/learn/drugs" />;

    return (
      <AnimatableView
        ref="baseView"
        animation="fadeInRight"
        duration={ 300 }
        easing="ease-in-out-sine"
        style={ BaseStyles.overlayContainer }>

        <Header back={ () => this._exit() } titleComponent={
          (
            <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
              <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_HEADINGS, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>Cari Obat</Text>
              <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, color: StyleConstants.COLOR_GRAY.LIGHTEST }} numberOfLines={ 1 }>
                { this.state.loaded && this.state.drugClass.name }
              </Text>
            </View>
          )
        } />

        {
          this.state.loaded &&
          (
            <ScrollView style={{ flex: 1 }}>
              <View style={{ height: StyleConstants.SPACING.X4 }} />
              {
                this.state.drugClass.subclasses.data.length ?
                (
                  <Fragment>
                    <View style={ BaseStyles.content }>
                      <Text style={ BaseStyles.h4 }>Subkelas</Text>
                    </View>
                    {
                      this.state.drugClass.subclasses.data.map(subclass =>
                        <SubclassListItem key={ subclass.code } drugClass={ this.state.drugClass.code } { ...subclass } />
                      )
                    }
                    <View style={{ height: StyleConstants.SPACING.X2 }} />
                  </Fragment>
                ) : null
              }
              {
                this.state.drugClass.drugs.data.length ?
                (
                  <Fragment>
                    <View style={ BaseStyles.content }>
                      <Text style={ BaseStyles.h4 }>Obat</Text>
                    </View>
                    {
                      this.state.drugClass.drugs.data.map(drug =>
                        <DrugListItem key={ drug.slug } { ...drug } referrer={ this.props.location.pathname } />
                      )
                    }
                    <View style={{ height: StyleConstants.SPACING.X2 }} />
                  </Fragment>
                ) : null
              }
              <View style={{ height: StyleConstants.SPACING.X5 }} />
            </ScrollView>
          )
        }
      </AnimatableView>
    );
  }
}

export default DrugClass;