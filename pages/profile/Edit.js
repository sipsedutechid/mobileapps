import React, { Component } from 'react';
import { BackHandler, KeyboardAvoidingView, ScrollView, Text, TextInput, View } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import { Redirect } from 'react-router-native';
import Touchable from 'react-native-platform-touchable';
import { CancelToken } from 'axios';

import ProfileApi from '../../api/profile';
import BaseStyles from '../../styles/base';
import * as StyleConstants from '../../styles/constants';
import Header from '../../components/Header';
import InputGroup from '../../components/InputGroup';
import RadioGroup from '../../components/RadioGroup';

class Edit extends Component {
  state = {
    exit: false,
    saving: false,
    errors: [],

    data: {
      name: '',
      password: '',
      passwordConfirm: '',
      profession: null,
      idNo: '',
      ...this.props.data
    }
  }
  exit = this._exit.bind(this);
  cancelToken = false;

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.exit);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ data: Object.assign({}, nextProps.data) })
  }

  save() {
    if (this.state.saving)
      return;

    this.setState({ saving: true, errors: [] });
    ProfileApi.update({
      accessToken: this.props.accessToken,
      data: this.state.data,
      cb: profile => {

        this.props.onSuccess(profile);
        this.cancelToken = false;
        this._exit();
      },
      err: errors => {
        this.setState({ saving: false, errors });
        this.cancelToken = false;
      },
      cancelToken: new CancelToken(c => { this.cancelToken = c; })
    })
  }

  _exit() {
    BackHandler.removeEventListener('hardwareBackPress', this.exit);
    this.cancelToken && this.cancelToken();
    this.refs.baseView.fadeOutRight(200);
    setTimeout(() => this.setState({ exit: true }), 200);
    return true;
  }

  render() {
    if (this.state.exit)
      return <Redirect to="/profile" />;

    return (
      <AnimatableView style={ BaseStyles.overlayContainer } animation="fadeInRight" duration={ 300 } easing="ease-in-out-sine" ref="baseView">
        <Header title="Ubah profil" back={ () => this.exit() } />

        <ScrollView style={{ flex: 1 }}>
          <KeyboardAvoidingView behavior="padding">
            <InputGroup label="Nama Lengkap" error={ this.state.errors.name && this.state.errors.name[0] }>
              <TextInput
                value={ this.state.data.name }
                style={ BaseStyles.textInput}
                onChange={ e => this.setState({ data: Object.assign(this.state.data, { name: e.nativeEvent.text }) }) }
                underlineColorAndroid="rgba(0,0,0,0)" />
            </InputGroup>
            <InputGroup label="Ubah Kata Sandi" error={ this.state.errors.password && this.state.errors.password[0] }>
              <TextInput
                value={ this.state.data.password }
                style={ BaseStyles.textInput}
                secureTextEntry={ true }
                onChange={ e => this.setState({ data: Object.assign(this.state.data, { password: e.nativeEvent.text }) }) }
                underlineColorAndroid="rgba(0,0,0,0)" />
            </InputGroup>
            <InputGroup label="Ulangi Kata Sandi" error={ this.state.errors.passwordConfirm && this.state.errors.passwordConfirm[0] }>
              <TextInput
                value={ this.state.data.passwordConfirm }
                style={ BaseStyles.textInput}
                secureTextEntry={ true }
                onChange={ e => this.setState({ data: Object.assign(this.state.data, { passwordConfirm: e.nativeEvent.text }) }) }
                underlineColorAndroid="rgba(0,0,0,0)" />
            </InputGroup>
            <InputGroup label="Profesi" error={ this.state.errors.profession && this.state.errors.profession[0] }>
              <RadioGroup
                onChange={ profession => this.setState({ data: Object.assign(this.state.data, { profession }) })}
                choices={[ { value: 'doctor', label: 'Dokter' }, { value: 'dentist', label: 'Dokter Gigi' } ]}
                selection={ this.state.data.profession } />
            </InputGroup>
            <InputGroup label="Nomor ID Keprofesian" error={ this.state.errors.idNo && this.state.errors.idNo[0] }>
              <TextInput
                value={ this.state.data.idNo }
                style={ BaseStyles.textInput}
                onChange={ e => this.setState({ data: Object.assign(this.state.data, { idNo: e.nativeEvent.text }) }) }
                underlineColorAndroid="rgba(0,0,0,0)" />
            </InputGroup>

          </KeyboardAvoidingView>

          <Touchable onPress={ () => this.save() } >
            <View style={{ paddingVertical: StyleConstants.SPACING.X3, alignItems: 'center' }}>
              <Text style={{ color: StyleConstants.COLOR.PRIMARY, fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, fontSize: StyleConstants.FONT_SIZE.H5 }}>Simpan</Text>
            </View>
          </Touchable>
        </ScrollView>
      </AnimatableView>
    );
  }
}

export default Edit;