import { Platform } from 'react-native';

// Colors
export const COLOR = {
  PRIMARY:  '#a4151a',
  WHITE:    '#ffffff',
  BLACK:    '#000000',
  GREEN:    '#43A047',
  AMBER:    '#FFB300',
  BLUE:     '#4f92ca'
};
export const COLOR_GRAY = {
  DARKEST:  '#263238',
  DARK:     '#455A64',
  NORMAL:   '#607D8B',
  LIGHT:    '#B0BEC5',
  LIGHTEST: '#ECEFF1',
};

// Typography
export const FONT_FAMILY_SANS_SERIF = 'Hind';
export const FONT_FAMILY_SERIF = 'PT Serif';
export const FONT_FAMILY_SERIF_ITALIC = Platform.OS === 'ios' ? 'PT Serif' : 'PT Serif Italic';
export const FONT_FAMILY_SERIF_STRONG = Platform.OS === 'ios' ? 'PT Serif' : 'PT Serif Bold';
export const FONT_FAMILY_SERIF_STRONG_ITALIC = Platform.OS === 'ios' ? 'PT Serif' : 'PT Serif Bold Italic';
export const FONT_FAMILY_HEADINGS = Platform.OS === 'ios' ? 'Montserrat' : 'Montserrat Bold';
export const FONT_SIZE_BASE = Platform.OS === 'ios' ? 16 : 14;
export const FONT_SIZE = {
  H1: FONT_SIZE_BASE + 24,
  H2: FONT_SIZE_BASE + 16,
  H3: FONT_SIZE_BASE + 8,
  H4: FONT_SIZE_BASE + 4,
  H5: FONT_SIZE_BASE + 2,
  H6: FONT_SIZE_BASE,
};
export const LINE_HEIGHT_BASE = 1.4;

// Spacing
export const SPACING_BASE = 4;
export const SPACING = {
  X2: SPACING_BASE * 2,
  X3: SPACING_BASE * 4,
  X4: SPACING_BASE * 6,
  X5: SPACING_BASE * 8,
};

// Borders
export const BORDER_RADIUS_BASE = 4;