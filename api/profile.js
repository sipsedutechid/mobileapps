import Axios from "axios";
import { BASE_URL, API_BASE_URL, API_CLIENT_ID, API_CLIENT_SECRET } from "../env";

export default class Profile {

  static show({ accessToken, cb, err }) {
    return Axios.get(API_BASE_URL + '/profile', {
      headers: {
        authorization: 'Bearer ' + accessToken
      }
    })
      .then(response => cb(response.data.data))
      .catch(e => err(e));
  }

  static update({ accessToken, data, cb, err, cancelToken }) {
    return Axios.post(API_BASE_URL + '/profile', data, {
      headers: { authorization: 'Bearer ' + accessToken },
      cancelToken
    })
      .then(response => cb(response.data.data))
      .catch(e => err(e.response.data));
  }

  static register({ data, cb, err, cancelToken }) {
    Axios.post(BASE_URL + '/api/user/register', data, {
      cancelToken
    })
      .then(response => cb())
      .catch(e => err(e.response.data));
  }

  static authenticate({ email, password, cb, err }) {
    const postData = {
      grant_type: 'password',
      client_id: API_CLIENT_ID,
      client_secret: API_CLIENT_SECRET,
      username: email,
      password,
      scope: '*',
    };

    return Axios.post(BASE_URL + '/oauth/token', postData)
      .then(response => cb({ access: response.data.access_token, refresh: response.data.refresh_token }))
      .catch(e => err(e));
  }

}