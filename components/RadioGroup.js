import React from 'react';
import { View } from 'react-native';
import RadioGroupItem from './RadioGroupItem';

const RadioGroup = props => (
  <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
    {
      props.choices.map((choice, index) => (
        <RadioGroupItem { ...choice } chosen={ choice.value === props.selection } key={ index } onSelect={ () => props.onChange(choice.value) } />
      ))
    }
  </View>
);

export default RadioGroup;