import React, { Fragment } from 'react';
import { Image, Text, View } from 'react-native';
import { Link } from 'react-router-native';
import Touchable from 'react-native-platform-touchable';

import ListStyles from '../../styles/lists';
import BaseStyles from '../../styles/base';

const ListItem = props => {

  const textBlock = (
    <Fragment>
      <Text style={ BaseStyles.h4 } numberOfLines={ 2 }>{ props.item.title }</Text>
      <Text style={ BaseStyles.text } numberOfLines={ 1 }>{ props.item.category.name }</Text>
      <Text style={ BaseStyles.muted } numberOfLines={ 1 }>{ props.item.publisher }, { props.item.issn }</Text>
    </Fragment>
  );

  return (
    <Link to={ '/learn/journals/' + props.item.slug } component={ Touchable }>
      <View style={ ListStyles.itemShadowed }>
        <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
          <Image source={{ uri: props.item.imageUrl }} style={{ height: 100, width: 80, marginRight: 24, borderRadius: 4 }} />
          <View style={{ flex: 1 }}>
            { textBlock }
          </View>
        </View>
      </View>
    </Link>
  );
}

export default ListItem;