import React from 'react';
import { Text, View } from 'react-native';

import BaseStyles from '../styles/base';
import * as StyleConstants from '../styles/constants';

const InputGroup = props => (
  <View style={{ paddingVertical: StyleConstants.SPACING.X3, borderBottomColor: StyleConstants.COLOR_GRAY.LIGHTEST, borderBottomWidth: 1 }}>
    <View style={ BaseStyles.content }>
      <Text style={ props.error ? BaseStyles.textLink : BaseStyles.text }>{ props.label }</Text>
      { props.children }
      {
        props.error &&
        <Text style={ BaseStyles.mutedSmall }>{ props.error }</Text>
      }
    </View>
  </View>
);

export default InputGroup;