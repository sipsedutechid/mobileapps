import React from 'react';
import { Image, View } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import * as StyleConstants from '../styles/constants';

const Spinner = props => (
  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingVertical: StyleConstants.SPACING.X3 }}>
    <AnimatableView
      animation="bounceIn"
      easing="ease-in-out"
      duration={ 900 }
      iterationCount="infinite"
      style={{
        width: props.size || 30,
        height: props.size || 30,
        borderRadius: props.size ? Math.floor(props.size) : 25,
        backgroundColor: StyleConstants.COLOR_GRAY.LIGHT,
        marginRight: (props.size || 30) / 10
        }} />
    <AnimatableView
      animation="bounceIn"
      easing="ease-in-out"
      duration={ 900 }
      iterationCount="infinite"
      delay={ 300 }
      style={{
        width: props.size || 30,
        height: props.size || 30,
        borderRadius: props.size ? Math.floor(props.size) : 25,
        backgroundColor: StyleConstants.COLOR_GRAY.LIGHT,
        marginRight: (props.size || 30) / 10
        }} />
    <AnimatableView
      animation="bounceIn"
      easing="ease-in-out"
      duration={ 900 }
      iterationCount="infinite"
      delay={ 600 }
      style={{
        width: props.size || 30,
        height: props.size || 30,
        borderRadius: props.size ? Math.floor(props.size) : 25,
        backgroundColor: StyleConstants.COLOR_GRAY.LIGHT,
        }} />
  </View>
);

export default Spinner;