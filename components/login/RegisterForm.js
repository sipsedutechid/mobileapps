import React, { Component } from 'react';
import { KeyboardAvoidingView, Image, Text, TextInput, View } from 'react-native';
import { View as AnimatableView } from 'react-native-animatable';
import Touchable from 'react-native-platform-touchable';
import { CancelToken } from 'axios';

import ProfileApi from '../../api/profile';
import LoginStyles from '../../styles/login';
import * as StyleConstants from '../../styles/constants';

class RegisterForm extends Component {
  state = {
    data: {
      name: '',
      email: '',
      password: '',
      passwordConfirmation: '',
    },
    errors: {},
    registering: false,
    authenticating: false,
  }
  cancelToken = false;

  register() {
    if (this.state.registering)
      return;

    this.setState({ registering: true, errors: {} });
    this.props.onSubmit();
    ProfileApi.register({
      data: this.state.data,
      cb: () => {
        this.cancelToken = false;
        return this.authenticate();
      },
      err: errors => {
        this.setState({ errors, registering: false });
        this.cancelToken = false;
        this.props.onFinish();
      },
      cancelToken: new CancelToken(c => { this.cancelToken = c; })
    });
  }

  authenticate() {
    this.setState({ authenticating: true, errors: {} });
    ProfileApi.authenticate({
      email: this.state.data.email,
      password: this.state.data.password,
      cb: ({ access, refresh }) => {
        this.props.onSuccess({ access, refresh });
      },
      err: e => {
        this.setState({
          errors: Object.assign(this.state.errors, { general: e.response.data.message }),
          authenticating: false,
          registering: false,
        });
        this.props.onFinish();
      }
    });
  }

  componentWillUnmount() {
    this.cancelToken && this.cancelToken();
  }

  render() {
    return (
      <AnimatableView animation="fadeInRight" duration={ 300 } easing="ease-in-out-circ" style={ LoginStyles.formContainer }>
        {
          this.state.errors.general &&
          (
            <View style={ LoginStyles.errorContainer }>
              <Text style={ LoginStyles.errorText }>{ this.state.errors.general }</Text>
            </View>
          )
        }
        <View style={{ marginBottom: StyleConstants.SPACING.X3, overflow: 'visible', width: '100%' }}>
          <KeyboardAvoidingView behavior="position" style={{ }}>
            <View style={ LoginStyles.inputContainer }>
              <Image source={ require('../../resources/images/User-Muted.png') } style={{ position: 'absolute', top: 13, left: 15, width: 20, height: 20 }} />
              <TextInput
                style={ LoginStyles.input }
                placeholder="Nama lengkap"
                keyboardType="default"
                autoCapitalize="sentences"
                underlineColorAndroid="rgba(0,0,0,0)"
                onFocus={ () => this.setState({ errors: Object.assign(this.state.errors, { name: false })})}
                onChange={ e => this.setState({ data: Object.assign(this.state.data, { name: e.nativeEvent.text }) }) } />
              { this.state.errors.name && <AnimatableView animation="fadeInRight" duration={ 300 } style={ LoginStyles.inputErrorContainer }><Text style={ LoginStyles.inputErrorText }>{ this.state.errors.name }</Text></AnimatableView> }
            </View>
            <View style={ LoginStyles.inputContainer }>
              <Image source={ require('../../resources/images/Envelope-Muted.png') } style={{ position: 'absolute', top: 13, left: 15, width: 20, height: 18 }} />
              <TextInput
                style={ LoginStyles.input }
                placeholder="Alamat email"
                keyboardType="email-address"
                autoCapitalize="none"
                underlineColorAndroid="rgba(0,0,0,0)"
                onFocus={ () => this.setState({ errors: Object.assign(this.state.errors, { email: false })})}
                onChange={ e => this.setState({ data: Object.assign(this.state.data, { email: e.nativeEvent.text }) }) } />
              { this.state.errors.email && <AnimatableView animation="fadeInRight" duration={ 300 } style={ LoginStyles.inputErrorContainer }><Text style={ LoginStyles.inputErrorText }>{ this.state.errors.email }</Text></AnimatableView> }
            </View>
            <View style={ LoginStyles.inputContainer }>
              <Image source={ require('../../resources/images/Lock-Muted.png') } style={{ position: 'absolute', top: 10, left: 15, width: 20, height: 22 }} />
              <TextInput
                style={ LoginStyles.input }
                placeholder="Kata sandi"
                autoCapitalize="none"
                secureTextEntry={ true }
                underlineColorAndroid="rgba(0,0,0,0)"
                onFocus={ () => this.setState({ errors: Object.assign(this.state.errors, { password: false })})}
                onChange={ e => this.setState({ data: Object.assign(this.state.data, { password: e.nativeEvent.text }) }) } />
              { this.state.errors.password && <AnimatableView animation="fadeInRight" duration={ 300 } style={ LoginStyles.inputErrorContainer }><Text style={ LoginStyles.inputErrorText }>{ this.state.errors.password }</Text></AnimatableView> }
            </View>
            <View style={ LoginStyles.inputContainer }>
              <Image source={ require('../../resources/images/Lock-Muted.png') } style={{ position: 'absolute', top: 10, left: 15, width: 20, height: 22 }} />
              <TextInput
                style={ LoginStyles.input }
                placeholder="Ulangi kata sandi"
                autoCapitalize="none"
                secureTextEntry={ true }
                underlineColorAndroid="rgba(0,0,0,0)"
                onFocus={ () => this.setState({ errors: Object.assign(this.state.errors, { passwordConfirmation: false })})}
                onChange={ e => this.setState({ data: Object.assign(this.state.data, { passwordConfirmation: e.nativeEvent.text }) }) } />
              { this.state.errors.passwordConfirmation && <AnimatableView animation="fadeInRight" duration={ 300 } style={ LoginStyles.inputErrorContainer }><Text style={ LoginStyles.inputErrorText }>{ this.state.errors.passwordConfirmation }</Text></AnimatableView> }
            </View>
          </KeyboardAvoidingView>
        </View>
        <Touchable onPress={ () => this.register() } style={ this.state.registering ? LoginStyles.submitBtnDisabled : LoginStyles.submitBtn }>
          <Text style={ LoginStyles.submitBtnText }>
            { (this.state.registering || this.state.authenticating) ? 'Mendaftar akun Anda..' : 'Buat Akun' }
          </Text>
        </Touchable>
      </AnimatableView>
    );
  }
}

export default RegisterForm;