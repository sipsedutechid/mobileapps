import React, { Component, Fragment } from 'react';
import { Image, ScrollView, Text, TouchableWithoutFeedback, View } from 'react-native';
import { Link } from 'react-router-native';
import Spinner from '../../components/Spinner';
import Error from '../../components/Error';

import BaseStyles from '../../styles/base';
import * as StyleConstants from '../../styles/constants';
import SliderListItem from './SliderListItem';

class HomepageSlider extends Component {
  state = {  }

  componentDidMount() {
    this.props.getArticles({ reload: true })
  }

  render() {
    return (
      <Fragment>
        <View style={{ height: StyleConstants.SPACING.X4 }} />
        <View style={ BaseStyles.content }>
          <Text style={ BaseStyles.h4 }>..baca artikel terhangat</Text>
        </View>
        <View style={{ height: StyleConstants.SPACING.X2 }} />

        <ScrollView horizontal={ true } showsHorizontalScrollIndicator={ false }>
          <View style={{ width: StyleConstants.SPACING.X4 }} />
          {
            this.props.articles.data.map(article => <SliderListItem key={ article.slug } { ...article } />)
          }
          <View style={{ width: StyleConstants.SPACING.X4 }} />
        </ScrollView>

        { this.props.articles.fetching && !this.props.articles.data.length && <Spinner /> }
        { this.props.articles.error && <Error onRetry={ () => this.props.getArticles({ reload: true }) } /> }

        {
          this.props.articles.data.length > 0 &&
          (
            <Link to="/articles" component={ TouchableWithoutFeedback }>
              <View style={ BaseStyles.content }>
                <Text style={ BaseStyles.textLink }>Semua artikel kesehatan</Text>
              </View>
            </Link>
          )
        }
      </Fragment>
    );
  }
}

export default HomepageSlider;