import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { requireNativeComponent, View } from 'react-native';

const VideoPlayer = () => (
  <ReactVideoPlayerView { ...this.props } />
);

VideoPlayer.propTypes = {
  ...View.propTypes,
  src: PropTypes.string,
};

const ReactVideoPlayerView = requireNativeComponent('ReactVideoPlayerView', VideoPlayer)

export default VideoPlayer;