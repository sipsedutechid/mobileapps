import React, {Component} from 'react';
import {Text, TouchableOpacity, View}from 'react-native';
import {Link} from 'react-router-native';

import * as StyleConstants from '../../styles/constants';
import BaseStyles from '../../styles/base';
import ListStyles from '../../styles/lists';
import {Avatar} from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";

class ListItem extends Component {

    content(){
        return(
            <View style={[this.props.item.isOpen ? ListStyles.itemShadowed : ListStyles.itemShadoweddis, {flexDirection: 'column', flex: 1}]}>
                <View style={{flex: 1, flexDirection: 'row', alignItems: 'flex-start'}}>
                    <Text style={{...BaseStyles.h4, flex: 0.60, color: 'black'}}
                          numberOfLines={2}>{this.props.item.teachableItem.data.title}</Text>
                    <Text style={{...BaseStyles.p, flex: 0.40, textAlign: 'right'}}
                          numberOfLines={1}>{this.props.item.createdAtForHumans}</Text>
                </View>
                <Text style={{...BaseStyles.p, flex: 0.25}}
                      numberOfLines={2}>{this.props.item.teachableItem.data.description}</Text>
                <View style={{flex: 1, flexDirection: 'row', alignItems: 'flex-start'}}>
                    {
                        this.props.item.type === 'assignment'
                            ? <Text style={{
                                ...BaseStyles.p, ...BaseStyles.badgeTopic,
                                backgroundColor: StyleConstants.COLOR.GREEN
                            }} numberOfLines={1}>Assignment</Text>
                            : this.props.item.type === 'quiz'
                            ? <Text style={{
                                ...BaseStyles.p, ...BaseStyles.badgeTopic,
                                backgroundColor: StyleConstants.COLOR.AMBER
                            }} numberOfLines={1}>Quiz</Text>
                            : <Text style={{
                                ...BaseStyles.p, ...BaseStyles.badgeTopic,
                                backgroundColor: StyleConstants.COLOR.BLUE
                            }} numberOfLines={1}>Resource</Text>
                    }
                    <Text style={{...BaseStyles.p, flex: 0.75, marginLeft: 10}} numberOfLines={1}>Deadline
                        : {this.props.item.expiresAtForHumans}</Text>
                </View>
                <View
                    style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: "space-between"}}>
                    <Text style={{textAlign: 'right', flex: 1, padding: 10}}>{this.props.item.createdBy.data.name}</Text>
                    <View>
                        <Avatar
                            rounded
                            source={{uri: this.props.item.createdBy.data.avatar}}/>
                    </View>
                </View>
                {
                    !this.props.item.isOpen
                        ? (<View style={{flexDirection:'row', justifyContent: 'flex-end', marginTop: 10}}>
                            <Icon name="lock" size={20} color={StyleConstants.COLOR_GRAY.LIGHT} />
                            <Text style={{textAlign: 'center', marginLeft: 10}}>Terkunci</Text>
                        </View>)
                        : (<View style={{flexDirection:'row', justifyContent: 'flex-end', alignItems: 'center', marginTop: 10}}>
                            <Text style={{textAlign: 'center', marginRight: 5, color: StyleConstants.COLOR.BLUE}}>Buka</Text>
                            <Icon name="chevron-right" size={10} color={StyleConstants.COLOR.BLUE} />
                        </View>)
                }
            </View>
        )
    }

    render() {
        return (
            <TouchableOpacity>
                {
                    this.props.item.isOpen
                        ? (<Link to={{
                            pathname: ('/class/' + this.props.slug + '/detail/' + this.props.item.id),
                            state: {typeTopic: this.props.item.type, teachableItem: this.props.item.teachableItem, idTopic: this.props.item.id, }}} >
                                {this.content()}
                           </Link>)
                        : this.content()
                }

            </TouchableOpacity>
        );
    }
}

export default ListItem;