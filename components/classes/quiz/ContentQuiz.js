import React, {Component} from 'react';
import {View, Text, ScrollView, Keyboard, TouchableWithoutFeedback} from 'react-native';

import QuizBoolean from './QuizBoolean';
import QuizMultipleChoice from "./QuizMultipleChoice";
import QuizMultipleRespon from "./QuizMultipleRespon";
import QuizEssay from './QuizEssay';
import QuizFillIn from "./QuizFillIn";

class ContentQuiz extends Component{

    render(){
        console.log(this.props.currAns);
        console.log(this.props.answer);

        let content = null;
        switch (this.props.data.type) {
            case 'Boolean': content = <QuizBoolean {...this.props.data.choiceItems} onSelect={this.props.onChange} answer={this.props.answer} currAns={this.props.currAns}/>; break;
            case 'MultipleChoice': content = <QuizMultipleChoice {...this.props.data.choiceItems} onSelect={this.props.onChange} answer={this.props.answer} currAns={this.props.currAns}/>; break;
            case 'MultipleRespon': content = <QuizMultipleRespon {...this.props.data.choiceItems} onSelect={this.props.onChange} answer={this.props.answer} currAns={this.props.currAns}/>; break;
            case 'Essay': content = <QuizEssay onChange={this.props.onChange} answer={this.props.answer} currAns={this.props.currAns}/>; break;
            case 'FillIn': content = <QuizFillIn onChange={this.props.onChange} answer={this.props.answer} currAns={this.props.currAns}/>; break;
        }

        return(
            <View style={{flex: 1, padding: 10, justifyContent: 'flex-start'}} onPress={Keyboard.dismiss} >
                <ScrollView>
                    <Text>{this.props.data.content}</Text>
                    {content}
                </ScrollView>
            </View>
        )
    }
}

export default ContentQuiz;