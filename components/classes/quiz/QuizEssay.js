import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import CNRichTextEditor, {CNToolbar, getDefaultStyles, getInitialObject, convertToHtmlString, convertToObject} from "react-native-cn-richtext-editor";

const defaultStyles = getDefaultStyles();

class QuizEssay extends Component{

    constructor(props){
        super(props);

        this.state = {
            selectedTag : 'body',
            selectedStyles : [],
            value: '',
        };

        this.editor = null;
    }

    componentDidMount(){
        if (this.props.answer === '' || this.props.answer === undefined){
            this.setState({value: [getInitialObject()]})
        }
        else{
            let answer = convertToObject(this.props.answer);
            this.setState({value: answer});
        }
    }

    onStyleKeyPress = (toolType) => {
        this.editor.applyToolbar(toolType);
    };

    onSelectedTagChanged = (tag) => {
        this.setState({
            selectedTag: tag
        })
    };

    onSelectedStyleChanged = (styles) => {
        this.setState({
            selectedStyles: styles,
        })
    };

    onValueChanged = (value) => {
        this.setState({
            value: value
        });
        this.props.onChange(convertToHtmlString(value));
    };


    render(){
        return (
            <View style={{flexDirection: 'column', justifyContent: 'flex-start', margin: 10}}>
                <View style={{width: 100, justifyContent: 'flex-start'}}>
                    <CNToolbar
                        size={16}
                        bold={<Text style={[styles.toolbarButton, styles.boldButton]}>B</Text>}
                        italic={<Text style={[styles.toolbarButton, styles.italicButton]}>I</Text>}
                        underline={<Text style={[styles.toolbarButton, styles.underlineButton]}>U</Text>}

                        selectedTag={this.state.selectedTag}
                        selectedStyles={this.state.selectedStyles}
                        onStyleKeyPress={this.onStyleKeyPress}/>
                </View>
                <View style={styles.compose}>
                    <View style={styles.composeText}>
                        <CNRichTextEditor
                            ref={input => this.editor = input}
                            style={{color: '#2cddab'}}
                            onSelectedTagChanged={this.onSelectedTagChanged}
                            onSelectedStyleChanged={this.onSelectedStyleChanged}
                            onValueChanged={this.onValueChanged}
                            value={this.state.value}
                            styleList={defaultStyles}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    composeText: {
        width: '100%',
        height: 200,
        marginLeft: 0,
        backgroundColor: 'white',
        borderColor: '#979797',
        borderStyle: 'solid',
        borderWidth: 1,
    },
    compose: {
        width: '100%',
        height: 200,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 0,
        marginRight: 0,
        marginBottom: 10,
    },
    toolbarButton: {
        fontSize: 16,
        width: 28,
        height: 28,
        textAlign: 'center'
    },
    italicButton: {
        fontStyle: 'italic'
    },
    boldButton: {
        fontWeight: 'bold'
    },
    underlineButton: {
        textDecorationLine: 'underline'
    },
    lineThroughButton: {
        textDecorationLine: 'line-through'
    },
});

export default QuizEssay;