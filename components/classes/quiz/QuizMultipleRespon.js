import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import * as StyleConstants from "../../../styles/constants";
import Icon from "react-native-vector-icons/FontAwesome";

class QuizMultipleChoice extends Component{

    state={
        arrKu : []
    };

    componentDidMount(){
        // this.setState({arrKu: [...this.props.answer]});
        // console.log("DataD: ", this.state.arrKu);
    }

    onPilih(data){
        this.state.arrKu.indexOf(data) > -1
            ? this.setState({arrKu: this.state.arrKu.filter((_,i) => i !== this.state.arrKu.indexOf(data))}, () => this.props.onSelect(this.state.arrKu))
            : this.setState({arrKu: [...this.state.arrKu, data]}, () => this.props.onSelect(this.state.arrKu));
    }

    render() {
        return (
            <View style={{flexDirection: 'column', justifyContent: 'flex-start', margin: 10}}>
                {
                    this.props.data.map((choice) => (
                        <TouchableOpacity onPress={() => this.onPilih(choice.id)} key={choice.id}>
                            <View style={{flex: 1, flexDirection: 'row', margin: 5}}>
                                {
                                    this.state.arrKu.indexOf(choice.id) > -1
                                        ? <Icon name="check-circle" size={20} color={StyleConstants.COLOR.BLUE}/>
                                        : <Icon name="circle" size={20} color={StyleConstants.COLOR_GRAY.LIGHT}/>
                                }
                                <Text style={{marginLeft: 10}}>{choice.choiceText}</Text>
                            </View>
                        </TouchableOpacity>
                    ))
                }
            </View>
        )
    }
}

export default QuizMultipleChoice;