import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import * as StyleConstants from "../../../styles/constants";
import Icon from "react-native-vector-icons/FontAwesome";

const QuizMultipleChoice = props => (
    <View style={{ flexDirection: 'column', justifyContent: 'flex-start', margin: 10}}>
        {
            props.data.map((choice) => (
                <TouchableOpacity onPress={ () => props.onSelect(choice.id) } key={choice.id}>
                    <View style={{flex: 1, flexDirection: 'row', margin: 5}}>
                        {
                            props.answer === props.currAns === choice.id
                                ? <Icon name="check-circle" size={20} color={StyleConstants.COLOR.BLUE} />
                                : props.currAns === choice.id
                                    ? <Icon name="check-circle" size={20} color={StyleConstants.COLOR.BLUE} />
                                    : <Icon name="circle" size={20} color={StyleConstants.COLOR_GRAY.LIGHT} />
                        }
                        <Text style={{marginLeft: 10}}>{choice.choiceText}</Text>
                    </View>
                </TouchableOpacity>
            ))
        }
    </View>
);

export default QuizMultipleChoice;