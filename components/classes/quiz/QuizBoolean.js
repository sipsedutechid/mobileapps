import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import * as StyleConstants from "../../../styles/constants";
import BaseStyles from "../../../styles/base";

const QuizBoolean = props => (
    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
        {
            props.data.map((choice) => (
                <TouchableOpacity onPress={ () => props.onSelect(choice.id) } key={choice.id}>
                    <View style={{...BaseStyles.promptButtonImage, marginTop: 20, marginHorizontal: 10}}>
                        {
                            props.answer === props.currAns === choice.id
                                ? <Icon name="check-circle" size={40} color={StyleConstants.COLOR.BLUE} />
                                : props.currAns === choice.id
                                    ? <Icon name="check-circle" size={40} color={StyleConstants.COLOR.BLUE} />
                                    : <Icon name="circle" size={40} color={StyleConstants.COLOR_GRAY.LIGHT} />
                        }
                        <Text style={{marginTop: 10}}>{choice.choiceText}</Text>
                    </View>
                </TouchableOpacity>
            ))
        }
    </View>
);

export default QuizBoolean;