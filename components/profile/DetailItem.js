import React from 'react';
import { Text, View } from 'react-native';
import * as StyleConstants from '../../styles/constants';

const DetailItem = props => (
  <View style={{ marginBottom: StyleConstants.SPACING.X3 }}>
    <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, fontSize: StyleConstants.FONT_SIZE_BASE, color: StyleConstants.COLOR_GRAY.NORMAL }} numberOfLines={ 1 }>{ props.title }</Text>
    <Text style={{ fontFamily: StyleConstants.FONT_FAMILY_SANS_SERIF, fontSize: StyleConstants.FONT_SIZE.H5, color: StyleConstants.COLOR_GRAY.DARKEST }}>{ props.detail }</Text>
  </View>
);

export default DetailItem;