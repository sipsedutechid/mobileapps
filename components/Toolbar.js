import React from 'react';
import { Image, Text, View } from 'react-native';
import { Link, Route, withRouter } from 'react-router-native';
import Touchable from 'react-native-platform-touchable';
import _ from 'lodash';

import BaseStyle from '../styles/base';

const Toolbar = props => {
  return (
    <View style={ BaseStyle.toolbar }>
      <Link to="/" component={ Touchable } style={ BaseStyle.toolbarTouchable }>
        <View style={ BaseStyle.toolbarItem }>
          {
            props.location.pathname === '/' ?
              <Image source={ require('../resources/images/HomeActive.png') } style={{ height: 30, width: 30 }} /> :
              <Image source={ require('../resources/images/HomeIdle.png') } style={{ height: 30, width: 30 }} />
          }
          <Text style={ props.location.pathname === '/' ? BaseStyle.toolbarItemTextActive : BaseStyle.toolbarItemText }>{'Beranda'.toUpperCase()}</Text>
        </View>
      </Link>
      <Link to="/articles" component={ Touchable } style={ BaseStyle.toolbarTouchable }>
        <View style={ BaseStyle.toolbarItem }>
          {
            _.startsWith(props.location.pathname, '/articles') ?
              <Image source={ require('../resources/images/ArticlesActive.png') } style={{ height: 30, width: 30 }} /> :
              <Image source={ require('../resources/images/ArticlesIdle.png') } style={{ height: 30, width: 30 }} />
          }
          <Text style={ _.startsWith(props.location.pathname, '/articles') ? BaseStyle.toolbarItemTextActive : BaseStyle.toolbarItemText }>{'Artikel'.toUpperCase()}</Text>
        </View>
      </Link>
      <Link to="/learn" component={ Touchable } style={ BaseStyle.toolbarTouchable }>
        <View style={ BaseStyle.toolbarItem }>
          {
            _.startsWith(props.location.pathname, '/learn') ?
              <Image source={ require('../resources/images/LearnActive.png') } style={{ height: 30, width: 30 }} /> :
              <Image source={ require('../resources/images/LearnIdle.png') } style={{ height: 30, width: 30 }} />
          }
          <Text style={ _.startsWith(props.location.pathname, '/learn') ? BaseStyle.toolbarItemTextActive : BaseStyle.toolbarItemText }>{'Belajar'.toUpperCase()}</Text>
        </View>
      </Link>
      {/* <Link to="/references" component={ Touchable } style={ BaseStyle.toolbarTouchable }>
        <View style={ BaseStyle.toolbarItem }>
          {
            _.startsWith(props.location.pathname, '/references') ?
              <Image source={ require('../resources/images/ArticlesActive.png') } style={{ height: 30, width: 30 }} /> :
              <Image source={ require('../resources/images/ArticlesIdle.png') } style={{ height: 30, width: 30 }} />
          }
          <Text style={ _.startsWith(props.location.pathname, '/references') ? BaseStyle.toolbarItemTextActive : BaseStyle.toolbarItemText }>{'Info'.toUpperCase()}</Text>
        </View>
      </Link> */}
      <Link to="/profile" component={ Touchable } style={ BaseStyle.toolbarTouchable }>
        <View style={ BaseStyle.toolbarItem }>
          {
            _.startsWith(props.location.pathname, '/profile') ?
              <Image source={ require('../resources/images/ProfileActive.png') } style={{ height: 30, width: 30 }} /> :
              <Image source={ require('../resources/images/ProfileIdle.png') } style={{ height: 30, width: 30 }} />
          }
          <Text style={ _.startsWith(props.location.pathname, '/profile') ? BaseStyle.toolbarItemTextActive : BaseStyle.toolbarItemText }>{'Profil'.toUpperCase()}</Text>
        </View>
      </Link>
    </View>
  );
}

export default withRouter(Toolbar);