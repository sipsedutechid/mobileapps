import React from 'react';
import {View, StyleSheet, Button, TextInput, Text} from 'react-native';
import CNRichTextEditor , { CNToolbar, getInitialObject , getDefaultStyles, convertToHtmlString, convertToObject } from "react-native-cn-richtext-editor";

import * as BaseConstants from '../../styles/constants';

const defaultStyles = getDefaultStyles();

class ComposeText extends React.Component{

    constructor(props){
        super(props);
        this.submit = this.submit.bind(this);

        this.state = {
            selectedTag : 'body',
            selectedStyles : [],
            value: [getInitialObject()],
            sendDisable: true,
        };

        this.editor = null;
    }

    submit(){

    }

    onStyleKeyPress = (toolType) => {
        this.editor.applyToolbar(toolType);
    };

    onSelectedTagChanged = (tag) => {
        this.setState({
            selectedTag: tag
        })
    };

    onSelectedStyleChanged = (styles) => {
        this.setState({
            selectedStyles: styles,
        })
    };

    onValueChanged = (value) => {
        this.setState({
            value: value
        });

        if (value[0].content[0].text.length > 0){
            this.setState({sendDisable: false});
        }
        else {
            this.setState({sendDisable: true});
        }
    };

    render() {
        return (
            <View style={{flexDirection: 'column', alignItems: 'center', backgroundColor: BaseConstants.COLOR_GRAY.LIGHTEST}}>
                <View>
                    <CNToolbar
                        size={28}
                        bold={<Text style={[styles.toolbarButton, styles.boldButton]}>B</Text>}
                        italic={<Text style={[styles.toolbarButton, styles.italicButton]}>I</Text>}
                        underline={<Text style={[styles.toolbarButton, styles.underlineButton]}>U</Text>}

                        selectedTag={this.state.selectedTag}
                        selectedStyles={this.state.selectedStyles}
                        onStyleKeyPress={this.onStyleKeyPress} />
                </View>

                <View style={styles.compose}>
                    <View style={styles.composeText}>
                        <CNRichTextEditor
                            ref={input => this.editor = input}
                            style={{color: '#2cddab'}}
                            onSelectedTagChanged={this.onSelectedTagChanged}
                            onSelectedStyleChanged={this.onSelectedStyleChanged}
                            onValueChanged={this.onValueChanged}
                            value={this.state.value}
                            styleList={defaultStyles}
                        />
                    </View>
                    <Button style={{marginRight: 10}} disabled={this.state.sendDisable} title={"Kirim"} onPress={this.submit()}/>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    composeText: {
        width: '80%',
        height: 50,
        marginLeft: 10,
        backgroundColor: 'white',
        borderColor: '#979797',
        borderStyle: 'solid',
        borderWidth: 1,
    },
    compose: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
    },
    toolbarButton: {
        fontSize: 20,
        width: 28,
        height: 28,
        textAlign: 'center'
    },
    italicButton: {
        fontStyle: 'italic'
    },
    boldButton: {
        fontWeight: 'bold'
    },
    underlineButton: {
        textDecorationLine: 'underline'
    },
    lineThroughButton: {
        textDecorationLine: 'line-through'
    },
});

export default ComposeText;