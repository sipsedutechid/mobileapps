import React from 'react';
import { Image, Platform, Text, View } from 'react-native';
import { Link } from 'react-router-native';
import Touchable from 'react-native-platform-touchable';

import BaseStyles from '../../styles/base'
import * as StyleConstants from '../../styles/constants'

const ListItem = props => {
  return (
    <Link
      to={ "/learn/p2kb/" + props.slug + '?section=p2kb' }
      component={ Touchable }
      style={{
        width:            170,
        overflow:         'hidden',
        elevation:        2,
        borderColor:      StyleConstants.COLOR_GRAY.LIGHTEST,
        borderWidth:      Platform.OS === 'ios' ? 1 : 0,
        marginRight:      props.index === props.topicsCount - 1 ? StyleConstants.SPACING.X3 * 4 : StyleConstants.SPACING.X3,
        borderRadius:     StyleConstants.SPACING.X2,
        marginBottom:     StyleConstants.SPACING.X3,
        backgroundColor:  StyleConstants.COLOR.WHITE,
      }}>
      <View>
        <Image source={{ uri: props.featuredImage }} style={{
          overflow: 'hidden',
          width: 170,
          height: 110,
          marginBottom: StyleConstants.SPACING.X3
          }} />
        <View style={{ paddingHorizontal: StyleConstants.SPACING.X3 }}>
          <Text style={ BaseStyles.muted } numberOfLines={ 1 }>Topik { props.release }</Text>
          <Text style={ BaseStyles.h6 } numberOfLines={ 3 }>{ props.title }</Text>
        </View>
      </View>
    </Link>
  );
}

export default ListItem;